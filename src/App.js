import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Customer from './components/Customer';
import AddNewCustomer from './components/AddNewCustomer';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Customer} />
        <Route path="/new" exact component={AddNewCustomer} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
