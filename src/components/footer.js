import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  footer: {
    padding: 10,
    textAlign: 'center'
  }
});

function Footer() {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <AppBar position="static" className={classes.footer}>
        Footer
      </AppBar>
    </div>
  );
}

export default Footer;
