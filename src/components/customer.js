import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import CustomerCard from './CustomerCard';
import Header from './Header';
import Footer from './Footer';

const useStyles = makeStyles({
  customer: {
    display: 'flex',
    flexDirection: 'row'
  }
});

function Customer() {
  const classes = useStyles();
  const [customers, setCustomers] = useState([]);

  async function fetchCustomers() {
    const res = await fetch('http://localhost:5000/api/customer-list');
    res.json().then(res => {
      setCustomers(res.customers);
    });
  }

  useEffect(() => {
    fetchCustomers();
  }, []);

  return (
    <>
      <Header />
      <Link to="/new">Add new customer</Link>
      <div className={classes.customer}>
        {customers.map(customer => (
          <CustomerCard
            key={customer.customer_id}
            customer={customer}
          ></CustomerCard>
        ))}
      </div>
      <Footer />
    </>
  );
}

export default Customer;
