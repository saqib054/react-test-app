import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  header: {
    padding: 10,
    marginBottom: 10,
    textAlign: 'center'
  }
});

function Header() {
  const classes = useStyles();
  return (
    <div className={classes.header}>
      <AppBar position="static" className={classes.header}>
        Test React App Header
      </AppBar>
    </div>
  );
}

export default Header;
