import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  card: {
    width: 275,
    margin: 10
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
});

function AddNewCustomer() {
  const [value, setValue] = useState([]);
  const classes = useStyles();

  const saveCustomer = async e => {
    e.preventDefault();
    if (!value) return;
    // TODO: Add method to call api
  };

  return (
    <div>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="h5" component="h2">
            <form>
              <label>Name</label>
              <input
                type="text"
                value={value}
                onChange={e => setValue(e.target.value)}
              />
            </form>
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={saveCustomer}>
            Save
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}

export default AddNewCustomer;
